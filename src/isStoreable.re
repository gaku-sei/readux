module type Sig = {
  type actions;

  type shape;

  let init: shape;

  let reducer: shape => actions => shape;
};
