module Functor (Module: IsStoreable.Sig) => {
  open Array;

  type listener = {id: int, cb: Module.shape => unit};

  let listeners: ref (array listener) = ref [||];

  let id: ref int = ref 0;

  let internalState = ref Module.init;

  let getState () => !internalState;

  let dispatch (action: Module.actions): unit => {
    internalState := Module.reducer Module.init action;
    !listeners |> iter (fun {cb} => getState () |> cb);
  };

  let subscribe cb: listener => {
    let item = {id: !id, cb};

    listeners := append !listeners [|item|];
    id := !id + 1;

    item;
  };

  let unsubscribe {id}: unit => {
    listeners := fold_left (fun acc listener =>
      listener.id == id ? acc : append acc [|listener|]) [||] !listeners;
  };
};
