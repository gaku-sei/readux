open Jest;

/*The whole Store initialization*/
module Actions = {
  type actions = NoOp | Reset | Set string;
};

module State = {
  type shape = string;

  let init = "coucou";
};

module Reducer = {
  open Actions;

  let reducer state => fun
    | NoOp => state
    | Reset => ""
    | Set newState => newState;
};

module MyStore = Store.Functor {
  include Actions;
  include State;
  include Reducer;
};

open Actions;
open MyStore;

let _ =

describe "Store" (fun () => {
  open Expect;

  let counter = ref 0;

  describe "NoOp" (fun () => {
    let listener = subscribe (fun state =>
      test "contains coucou" (fun () =>
        expect state |> toBe "coucou"));

    dispatch NoOp;

    unsubscribe listener;
  });

  describe "Reset" (fun () => {
    let listener = subscribe (fun state =>
      test "contains nothing" (fun () =>
        expect state |> toBe ""));

    dispatch Reset;

    unsubscribe listener;
  });

  describe "Set" (fun () => {
    let listener = subscribe (fun state =>
      test "contains recoucou" (fun () =>
        expect state |> toBe "recoucou"));

    dispatch (Set "recoucou");

    unsubscribe listener;
  });
});
